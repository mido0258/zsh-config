# My personal ZSH configuration

This repository contains only zshrc file


## Credits and used links

- [https://grml.org/zsh/](https://grml.org/zsh/)
- [https://github.com/akarzim/zsh-docker-aliases](https://github.com/akarzim/zsh-docker-aliases)

## Depencies

 - Oh my zsh 
 - Powerlevel9k 
 - zsh completion
